var listSv = [];

var dataJson = localStorage.getItem("DSSV");
// convert từ js sang json

// console.log(dataJson);

if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  // convert từ json sang js

  listSv = dataRaw.map(function (item) {
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
    return sv;
  });
  // convert dâta từ local để lấy các method ( Obj lấy từ local sẽ mất method)
  render(listSv);
}

function saveLocalStorage() {
  var data = JSON.stringify(listSv);
  localStorage.setItem("DSSV", data);
}

function themSV() {
  var sv = layThongTinTuForm();
  var isValid = true;
  isValid =
    isValid & inputRequire(sv.ma, "spanMaSV") &&
    checkIdentification(sv.ma, listSv, "spanMaSV");
  isValid = isValid & inputRequire(sv.ten, "spanTenSV");
  isValid =
    isValid & inputRequire(sv.email, "spanEmailSV") &&
    checkEmail(sv.email, "spanEmailSV");
  isValid =
    isValid & inputRequire(sv.matKhau, "spanMatKhau") &&
    lengthPassword(sv.matKhau, "spanMatKhau", 6, 12);
  isValid =
    isValid & inputRequire(sv.diemToan, "spanToan") &&
    checkNumber(sv.diemToan, "spanToan");
  isValid =
    isValid & inputRequire(sv.diemLy, "spanLy") &&
    checkNumber(sv.diemLy, "spanLy");
  isValid =
    isValid & inputRequire(sv.diemHoa, "spanHoa") &&
    checkNumber(sv.diemHoa, "spanHoa");

  if (!isValid) return;

  listSv.push(sv);
  saveLocalStorage();
  render(listSv);
  resetForm();
}

function XoaSv(idSv) {
  var index = listSv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) {
    return;
  }
  listSv.splice(index, 1);
  saveLocalStorage();
  render(listSv);
}
function SuaSv(idSv) {
  var index = listSv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) {
    return;
  }

  var sv = listSv[index];
  hienThiThongTinLenForm(sv);
  disableInput();
}

function capNhatSv() {
  var editSV = layThongTinTuForm();
  var index = listSv.findIndex(function (sv) {
    return sv.ma == editSV.ma;
  });
  if (index == -1) {
    return;
  }
  // replace khi sửa thông tin bằng edit Sv
  listSv[index] = editSV;
  saveLocalStorage();
  render(listSv);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
}

function search(value) {
  var inputElement = document.getElementById("txtSearch").value.trim();
  value = inputElement;
  return value.ten.substring(0, key.length) == key;
}
var result = listSv.filter(search);
