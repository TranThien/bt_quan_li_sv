function layThongTinTuForm() {
  var maSV = document.getElementById("txtMaSV").value.trim();
  var tenSV = document.getElementById("txtTenSV").value.trim();
  var email = document.getElementById("txtEmail").value.trim();
  var matKhau = document.getElementById("txtPass").value.trim();
  var diemToan = document.getElementById("txtDiemToan").value.trim();
  var diemLy = document.getElementById("txtDiemLy").value.trim();
  var diemHoa = document.getElementById("txtDiemHoa").value.trim();

  var sv = new SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa);
  return sv;
}

function render(list) {
  var htmls = "";
  for (var i = 0; i < list.length; i++) {
    var currentSv = list[i];
    var content = `<tr>
          <td>${currentSv.ma}</td>
          <td>${currentSv.ten}</td>
          <td>${currentSv.email}</td>
          <td>${currentSv.DTB()}</td>
          <td>
          <button onclick="XoaSv('${
            currentSv.ma
          }')" class="btn btn-danger">Xóa</button>
          <button onclick="SuaSv('${
            currentSv.ma
          }')" class="btn btn-success">Sửa</button>
          </td>
          </tr>`;

    htmls += content;
  }
  document.querySelector("#tbodySinhVien").innerHTML = htmls;
}

function hienThiThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}

function disableInput() {
  document.getElementById("txtMaSV").disabled = true;
}
