function inputRequire(value, Error) {
  if (!value) {
    document.getElementById(Error).innerHTML = "Vui lòng nhập trường này";
    return false;
  } else {
    document.getElementById(Error).innerHTML = "";
    return true;
  }
}
function checkIdentification(idSv, listSv, errorMessage) {
  var index = listSv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) {
    document.getElementById(errorMessage).innerHTML = "";
    return true;
  } else {
    document.getElementById(errorMessage).innerHTML = "Mã sinh viên đã tồn tại";
    return false;
  }
}

function checkEmail(email, errorMessage) {
  var re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var check = re.test(email);

  if (!check) {
    document.getElementById(errorMessage).innerHTML =
      "Trường này phải là Email";
    return false;
  } else {
    document.getElementById(errorMessage).innerHTML = "";
    return true;
  }
}

function lengthPassword(value, errorMessage, minLength, maxLength) {
  if (value.length >= minLength && value.length < maxLength) {
    document.getElementById(errorMessage).innerHTML = "";
    return true;
  } else {
    document.getElementById(
      errorMessage
    ).innerHTML = `Vui lòng nhập tối thiểu ${minLength} - ${maxLength} kí tự`;
    return false;
  }
}

function checkNumber(number, errorMessage) {
  var re = /^[0-9]+$/;

  var check = re.test(number);

  if (!check) {
    document.getElementById(errorMessage).innerHTML = `Trường này phải là số`;
    return false;
  } else {
    document.getElementById(errorMessage).innerHTML = "";
    return true;
  }
}
