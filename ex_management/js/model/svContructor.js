function SinhVien(ma, ten, email, matKhau, diemToan, diemLy, diemHoa) {
  this.ma = ma;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.diemToan = diemToan;
  this.diemLy = diemLy;
  this.diemHoa = diemHoa;

  this.DTB = function () {
    var result = (this.diemToan * 1 + this.diemLy * 1 + this.diemHoa * 1) / 3;

    return result.toFixed(1);
  };
}
